<?php


use Silex\Provider\WebProfilerServiceProvider;


// Include prod configuration
require __DIR__.'/prod.php';


// Enable debug mode
$app['debug'] = true;


$app->register(new WebProfilerServiceProvider(), array(
    'profiler.cache_dir' => __DIR__.'/../var/cache/profiler',
));
