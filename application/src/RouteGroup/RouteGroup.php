<?php

namespace FacelessCreative\LBDemo\RouteGroup;


interface RouteGroup
{

    /**
     * Mount the route collection
     *
     * @param \Silex\Application $app
     * @param string $mountPoint
     * @return
     */
    public function apply(\Silex\Application $app, string $mountPoint);

}
