<?php


namespace FacelessCreative\LBDemo\RouteGroup;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class LadbrokesTestRouteGroup implements RouteGroup
{

    public function apply(\Silex\Application $app, string $mountPoint)
    {


        /** @var \Silex\ControllerCollection $ladbrokesDummyControllers */
        $ladbrokesDummyControllers = $app['controllers_factory'];


//====================================================

        /**
         * Returns a list of upcoming races
         */
        $ladbrokesDummyControllers->get('/events', function () use ($app) {

            $serverTime = new \DateTime();

            $serverTimeAsString = $serverTime->format('c');

            // Generate race times for our test API call
            $raceTimes = array();
            $raceTimeFromNow = array(
                1 => 'PT1M',
                2 => 'PT5M',
                3 => 'PT22M',
                4 => 'PT1H4M',
                5 => 'PT1H10M',
                6 => 'PT2H15M',
                7 => 'PT2H22M',
            );
            for($raceTime = 1; $raceTime <= 7; $raceTime++) {
                $currentTime = new \DateTime();
                $raceTimes[$raceTime] = $currentTime->add(new \DateInterval($raceTimeFromNow[$raceTime]));
            }


            $response = array(
                "success" => true,
                "apiTime" => $serverTimeAsString,
                "races" => array(
                    array(
                        "name" => "Ascott Race 4",
                        'category' => "Greyhound",
                        "suspend" => $raceTimes[1]->format('c'),
                        "eventId" => "1",
                        "timeRemaining" => "calculating"
                    ),
                    array(
                        "name" => "Darwin Race 2",
                        'category' => "Harness",
                        "suspend" => $raceTimes[2]->format('c'),
                        "eventId" => "2",
                        "timeRemaining" => "calculating"
                    ),
                    array(
                        "name" => "Ipswich Race 3",
                        'category' => "Thoroughbred",
                        "suspend" => $raceTimes[3]->format('c'),
                        "eventId" => "3",
                        "timeRemaining" => "calculating"
                    ),
                    array(
                        "name" => "Ascot Race 9",
                        'category' => "Greyhound",
                        "suspend" => $raceTimes[4]->format('c'),
                        "eventId" => "4",
                        "timeRemaining" => "calculating"
                    ),
                    array(
                        "name" => "Flemington Race 3",
                        'category' => "Harness",
                        "suspend" => $raceTimes[5]->format('c'),
                        "eventId" => "5",
                        "timeRemaining" => "calculating"
                    ),
                    array(
                        "name" => "Flemington Race 4",
                        'category' => "Thoroughbred",
                        "suspend" => $raceTimes[6]->format('c'),
                        "eventId" => "6",
                        "timeRemaining" => "calculating"
                    ),
                    array(
                        "name" => "Horsham Race 10",
                        'category' => "Greyhound",
                        "suspend" => $raceTimes[7]->format('c'),
                        "eventId" => "7",
                        "timeRemaining" => "calculating"
                    ),
                )

            );


            return $app->json($response, ($response['success'] == true ? Response::HTTP_OK : Response::HTTP_BAD_REQUEST));

        });


        /**
         * Returns test data for each of the Event IDs exposed by the /events GET route
         */
        $ladbrokesDummyControllers->get('/event/{eventId}', function ($eventId) use ($app) {


            switch ($eventId) {
                case 1:
                    $participants = array(
                        array(
                            "place" => "7",
                            "name" => "One for tiger",
                            "other" => "T: Nathan Goodwin"
                        ), array(
                            "place" => "2",
                            "name" => "Despicable Betty",
                            "other" => "T: Kerry Mansley"
                        ), array(
                            "place" => "3",
                            "name" => "Me ginger",
                            "other" => "T: Shake Ebek"
                        ), array(
                            "place" => "4",
                            "name" => "Bullbar Babie",
                            "other" => "T: Andrew Bell"
                        ), array(
                            "place" => "5",
                            "name" => "Angela's Star",
                            "other" => "T: Dennis Stewart"
                        )
                    );

                    break;
                case 2:
                    $participants = array(
                        array(
                            "place" => "7",
                            "name" => "Sheer Strength",
                            "other" => "J: Beau Appo W: 58.4 KGS"
                        ), array(
                            "place" => "2",
                            "name" => "Levi Jade",
                            "other" => "J: K Yoshida W: 58 KGS"
                        ), array(
                            "place" => "3",
                            "name" => "Long road to Fame",
                            "other" => "J: M Murphy W: 57.7 KGS"
                        ), array(
                            "place" => "4",
                            "name" => "Slipaslygrin",
                            "other" => "J: M MCGLLVRAY W: 56 KGS"
                        ), array(
                            "place" => "5",
                            "name" => "Son of Mara",
                            "other" => "J: N Keal W: 55 KGS"
                        ), array(
                            "place" => "6",
                            "name" => "Usane Flash",
                            "other" => "J: B Stower W: 54 KGS"
                        ), array(
                            "place" => "1",
                            "name" => "Croft Bay",
                            "other" => "J: S Apthorpe W: 54 KGS"
                        )
                    );
                    break;
                case 3:
                    $participants = array(
                        array(
                            "place" => "7",
                            "name" => "Gigante",
                            "other" => "J: Eric Barb W: 52.62 KGS"
                        ), array(
                            "place" => "2",
                            "name" => "State Solicitor",
                            "other" => "J: Luis Quinones W: 54.62 KGS"
                        ), array(
                            "place" => "3",
                            "name" => "Profit Street",
                            "other" => "J: Joe Blow W: 10 KGS"
                        ), array(
                            "place" => "4",
                            "name" => "Mad Brad",
                            "other" => "J: Wilkin Ortiz W: 54.43 KGS"
                        ), array(
                            "place" => "5",
                            "name" => "Miki Two Toes",
                            "other" => "J: Kevin Gnzalez W: 55 KGS"
                        ), array(
                            "place" => "6",
                            "name" => "My Greek Boy",
                            "other" => "J: Sam Bermuz W: 56 KGS"
                        ), array(
                            "place" => "1",
                            "name" => "Let it Slip",
                            "other" => "J: Luciano Hernan W: 53 KGS"
                        )
                    );
                    break;
                case 4:
                    $participants = array(
                        array(
                            "place" => "7",
                            "name" => "Johnny Be Quick",
                            "other" => "T: C Healey"
                        ), array(
                            "place" => "2",
                            "name" => "Palace Tycoon",
                            "other" => "T: T J McIerney"
                        ), array(
                            "place" => "3",
                            "name" => "Sankara Spirit",
                            "other" => "T: Lane & Wallice"
                        ), array(
                            "place" => "4",
                            "name" => "I've got friends",
                            "other" => "T: J T McIerney"
                        ), array(
                            "place" => "5",
                            "name" => "Code One",
                            "other" => "T: J T McIerney"
                        ), array(
                            "place" => "6",
                            "name" => "Trump It",
                            "other" => "T: J T McIerney"
                        ), array(
                            "place" => "1",
                            "name" => "Driving Wheels",
                            "other" => "T: Niera Spotter"
                        )
                    );
                    break;
                case 5:
                    $participants = array(
                        array(
                            "place" => "7",
                            "name" => "Major Kiwi",
                            "other" => "D: Scott Woog W: 88 KGS"
                        ), array(
                            "place" => "2",
                            "name" => "Its Bettor Me",
                            "other" => "D: Chris Offcut W: 77 KGS"
                        ), array(
                            "place" => "3",
                            "name" => "A Good Chance",
                            "other" => "D: Tim Offcut W: 81.45 KGS"
                        ), array(
                            "place" => "4",
                            "name" => "Pepperall",
                            "other" => "D: Russel Foste W: 79.10 KGS"
                        ), array(
                            "place" => "5",
                            "name" => "Lester Oh Jester",
                            "other" => "D: Jeff Staff W: 90.10 KGS"
                        ), array(
                            "place" => "6",
                            "name" => "Cold Ideal",
                            "other" => "D: Roger Planter W: 68.04 KGS"
                        ), array(
                            "place" => "1",
                            "name" => "Monumental",
                            "other" => "D: Michelle Green W: 90.67 KGS"
                        )
                    );
                    break;
                case 6:
                    $participants = array(
                        array(
                            "place" => "7",
                            "name" => "Rocketbll",
                            "other" => "J: Jerry Vargis W: 62.89 KGS"
                        ), array(
                            "place" => "2",
                            "name" => "Kings Archer",
                            "other" => "J: Matthew McOnhey W: 55 KGS"
                        ), array(
                            "place" => "3",
                            "name" => "Unagi",
                            "other" => "J: Oscar Florrets W: 56.54 KGS"
                        ), array(
                            "place" => "4",
                            "name" => "Bankable Teddy",
                            "other" => "J: Gustav Gerosi W: 67.77 KGS"
                        ), array(
                            "place" => "5",
                            "name" => "Cashin",
                            "other" => "J: Freddy Fried W: 82 KGS"
                        ), array(
                            "place" => "6",
                            "name" => "Redberry Lane",
                            "other" => "J: Yamil Roserie W: 55.34 KGS"
                        ), array(
                            "place" => "1",
                            "name" => "Bold Viking",
                            "other" => "J: Matt McGowan W: 56.87 KGS"
                        )
                    );
                    break;
                case 7:
                    $participants = array(
                        array(
                            "place" => "7",
                            "name" => "Oppa Short",
                            "other" => "T: C Cheachy"
                        ), array(
                            "place" => "2",
                            "name" => "Archer",
                            "other" => "T: J T McInerey"
                        ), array(
                            "place" => "3",
                            "name" => "Homebush Patriot",
                            "other" => "T: Lane & Wallice"
                        ), array(
                            "place" => "4",
                            "name" => "Teddy Two",
                            "other" => "T: T Heale"
                        ), array(
                            "place" => "5",
                            "name" => "Cashin Meats",
                            "other" => "T: Rov Hamilton"
                        ), array(
                            "place" => "6",
                            "name" => "Merry Lane",
                            "other" => "T: John Allice"
                        ), array(
                            "place" => "1",
                            "name" => "Victory Vicious",
                            "other" => "T: J T McImneresy"
                        )
                    );
                    break;
                default:
                    $participants = array(
                        array(
                            "place" => "7",
                            "name" => "Default #1"
                        ), array(
                            "place" => "2",
                            "name" => "Default #2"
                        ), array(
                            "place" => "3",
                            "name" => "Default #3"
                        ), array(
                            "place" => "4",
                            "name" => "Default #4"
                        ), array(
                            "place" => "5",
                            "name" => "Default #5"
                        ), array(
                            "place" => "6",
                            "name" => "Default #6"
                        ), array(
                            "place" => "1",
                            "name" => "Default #7"
                        )
                    );
            }


            $response = array(
                "success" => true,
                "participants" => $participants
            );


            return $app->json($response, ($response['success'] == true ? Response::HTTP_OK : Response::HTTP_BAD_REQUEST));

        });


//====================================================


        $app->mount($mountPoint, $ladbrokesDummyControllers);

    }

}
