#!/usr/bin/env bash

# Config shell
echo -e "\n\nalias ls='ls -alhs --color=auto'" >> /home/ubuntu/.bashrc


# Update APT and install required packages
apt-get update


# Install basic utils
apt-get install -y curl git unzip


# Install apache and setup modules
apt-get install -y apache2
a2enmod rewrite ssl
cat /vagrant/environment/vagrant/apache2/apache2.conf > /etc/apache2/apache2.conf
cat /vagrant/environment/vagrant/apache2/000-default.conf > /etc/apache2/sites-available/000-default.conf
cat /vagrant/environment/vagrant/apache2/default-ssl.conf > /etc/apache2/sites-available/default-ssl.conf


# Install and setup php 7
apt-get install -y php7.0 libapache2-mod-php7.0
apt-get install -y php7.0-pgsql php7.0-intl php-xdebug php7.0-mcrypt php7.0-curl
cat /vagrant/environment/vagrant/php-7.0/apache2/php.ini > /etc/php/7.0/apache2/php.ini
cat /vagrant/environment/vagrant/php-7.0/cli/php.ini > /etc/php/7.0/cli/php.ini
cat /vagrant/environment/vagrant/php-7.0/xdebug.ini > /etc/php/7.0/mods-available/xdebug.ini


# Setup composer
echo -e "\n\nexport PATH=/home/ubuntu/.composer/vendor/bin:$PATH" >> /home/ubuntu/.bashrc
curl -s http://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer


# Restart LAMP services
service apache2 restart


## Install composer deps
su ubuntu -c "cd /vagrant/application; composer install --prefer-dist;"
