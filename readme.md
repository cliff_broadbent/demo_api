**What is this**
-
This is a simple, CORS enabled API back-end written in PHP for an example "Next 5 Races" web app.

It provides the following routes:

- GET /ladbrokes/events
- GET /ladbrokes/event/{{ id }}

It returns JSON responses.

It is intended to be used with the `demo_client` webapp.

See:

- `http://bitbucket.org/cliff_broadbent/demo_client`

**Prerequisites**
-
An internet connection is required for the build process.

You will also require the following software installed:

- VirtualBox
- Vagrant
- Git


**To use this Demo API**
-
Running this demo is automated. Follow these steps:

- Clone the git repository to your local machine;
- Ensure you have the above prerequisites installed;
- Once the git clone has completed, navigate into the new directory;
- Execute the `vagrant up` command;
- Wait for the machine provisioning to complete;
- Once complete, host will be `http://localhost:8010/`

**If things go wrong**
-

You can access the vagrant machine by:

- Navigating to the demo directory;
- Executing `vagrant ssh`.
 
Most commonly, the fault lies in either:

- The requested port map failing as the host already uses said port;
- The PHP Composer dependencies fail to install.
 